# Preparing for UHack

Whilst you're not allowed to actually work on your project until the weekend of UHack itself, that doesn't mean you can't be preparing and learning new skills and tools beforehand.

During UHack weekend you'll be working on your Business Model Canvas, your app and your pitch video. You must not do any of this work prior to the event.

Leading up to the event it is fine for your team to start thinking about the problems and opportunities you want to address. Do your research into the [University Research Themes](http://www.utas.edu.au/research/our-research-themes) and spend some time digging around the [University website](http://www.utas.edu.au).

It's also a good idea to learn about the [Business Model Canvas](https://www.google.com.au/?q=business%20model%20canvas) and do some research into preparing pitch videos. 

<!--We'll be running some seminars on Business Model Canvas leading up to UHack weekend – keep an eye out on the UHack website and the University Event Calendar for details – stay tuned!-->

**Finally, it's a very good idea for your team to familiarise yourself with your development and video editing tools, and with your hosting platform of choice before UHack weekend!**


