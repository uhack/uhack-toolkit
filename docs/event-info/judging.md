# Judging

Judging will take place at the end of UHack Weekend. It is very important that your project materials and app remain accessible via the public Internet during this time.

All UHack entries will be judged by the UHack Competition Judging Panel against the following criteria:

## The Business Model Canvas (15%)

  Are you able to get your business model on one page, that makes it easy to understand: the unmet need, the users / customers, what's unique about your solution, how you will get it your solution to the users, how you expect to cover your costs and beyond, and what's your unfair advantage? Your business model canvas will also show how your solution aligns with one of the University's research themes.

## Your App (50%)

  How well executed is your app, game or website, etc.? What is the User Experience like? Have you used any unique technical approaches? Is it accessible? How is your code and solution structured? Hint: If you don't have a working app, do you have detailed concept designs?

## The Pitch Video (15%)
  Are you able to communicate your business canvas, demonstrate your app, and engage would-be users in 3 minutes? Can you use the video medium to create excitement and demand for your solution? What evidence can you provide or discuss that adds credibility to your team and your solution? Do you need more resources to take it to the next level – would your video attract crowd funding, the interest of an angel investors or philanthropist – or just get you on the news?

## Innovation (10%)
  Do you solve a problem or capture an opportunity in a creative or never-seen-before way? How novel is your idea?

## Potential for Impact (10%)
  Would the application and associated plan have real impact (economic, societal, environmental)? Can it change the world, or at least help someone in your community? Does it solve a problem aligned with the University's research themes?

## Important: Your Pitch Video is one of the main vehicles for explaining your business model and demonstrating your app to the judges.
Due to time restrictions, your team won't be able to demo your app fully to the judges. Your pitch video will be viewed by the judges, and used to assess your business model, your app, innovation and potential for impact. 