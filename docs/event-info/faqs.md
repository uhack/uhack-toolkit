#FAQs

##Who can compete in UHack?
UHack is a state wide, open hackathon competition. UHack Tasmania is open to anyone including students and the public. If you are under 18 you must be accompanied by a parent or guardian.

##What will my team do during UHack?
Your team will need to produce three main deliverables during UHack weekend:

* A Business Model Canvas (BMC) that lays out the elements of how your business idea and app could be brought to reality. We'll provide plenty of BMC worksheets for you to use during the weekend. When you are finished you'll have to upload an image of your canvas somewhere it can be accessed by our judges, (e.g. Google Docs, OneDrive, DropBox), and link to it from your DevPost project page.
* A Software Application. This can be anything, e.g. a website, a mobile app, a game. **You'll need to host your app somewhere that it can be viewed or downloaded by our judges during UHack weekend (and longer if possible), and link to it from your DevPost project page.** We suggest you use one of the many free or trial services available from the likes of Google, Azure, OpenShift, Heroku, GitHub, etc. (NB: UHack does not provide any app hosting facilities.) **Tip: your team should familiarise themselves with your hosting platform and development tools before the competition weekend.**
* A 3-Minute Pitch Video. During your video you'll summarise your Business Model Canvas and demo your app. You'll need to shoot and edit your video using your own equipment – laptop and phone camera quality will be fine. You'll need to upload your video to YouTube (or similar) for our judges to view, and link to it from your DevPost project page.

**Important: your pitch video will be one of the main vehicles used by our judges to score your entry. Due to time constraints there may not be the chance to demonstrate your app fully to the judges, so it is important that your pitch video does a good job of demoing your app as well as summarising your business model. Also, make sure you tie your entry to one or more of the University's Research Themes.**

What you develop is up to you – so long as the problem and opportunity that you identify aligns with the University Research Themes:

* Environment, Resources and Sustainability
* Creativity, Culture and Society
* Better Health
* Marine, Antarctic and Maritime
* Data, Knowledge and Decisions


##Are we allowed to start our work before the competition starts?
No, you are not allowed. Whilst the event will be an opportunity to apply what you've learnt from browsing APIs, datasets or websites prior to the event, the event will be relying on you commencing work only from the Saturday morning. The only exception to this rule is if it's been made Open Source, freely available for use by other teams and in the public-domain.

Prior to the event, feel free to fill your boots with learning about relevant University research, themes, start thinking about problems aligned with the themes, etc., all of which will help you once the event kicks off and perhaps give you the edge you need. But don't pre-build - if we detect it, your entry will be jeopardised.

##Can I stay overnight?
<!--You don't have to stay overnight, however you will not be allowed to work on your project at any venue other than the competition venues, and if you leave after 10:00PM you will not be able to re-enter the venue until 7 am. -->
Due to venue, staffing and security restrictions, we won't be able to keep the venues open all night. The UHack venues will close at 11:00pm on Friday and Saturday night, reopening at 7:00am the next morning.

##I have never entered a hackathon before. Do you provide any help before the Hackathon?
Leading up to the competition there are a number of seminars. Check the main website for details. 

<!--Register for the seminars [here](https://www.eventbrite.com.au/d/australia--hobart/uhack/?mode=search).-->

<!--We'll also have a team of mentors available during the event to provide you with advice. -->

##What is the minimum & maximum number of people in a team?
We recommend at least four (4) people, with a maximum of six (6) people in your team.

##Who owns my creations?
Any intellectual property (e.g. code, images, audio, business plans, pitch videos) that you and/or your team create during the competition belongs to you and/or your team. A condition of entry is that you give permission to the University to reproduce and display your business models and videos – we'll use these to showcase your achievements and UHack after the event.

##I don't have a team or a full team yet, what do I do?

Best thing is to head over to the UHack Facebook page ([facebook.com/uhacktas](https://facebook.com/uhacktas)) and post that you are looking for a team.

<!--
You can register by yourself or as an incomplete team. Make sure you fill in your details and come to our meetup event to form a full team before the event, and do some fun team building activities! The meetup is happening on Tuesday 23/8 - see EventBrite for details: [https://www.eventbrite.com.au/e/uhack-meetup-tickets-27018220224](https://www.eventbrite.com.au/e/uhack-meetup-tickets-27018220224).
-->

##What should I bring?
You'll need to bring all the equipment you need to do your work, including laptops, mobile phone, chargers, and a change of clothes. Make sure you also bring the cables that connect your phones to your computers - you'll need these to transfer videos and pictures. Make sure you bring warm clothes too.

##What is provided?
We'll provide WiFi, breakfast, snacks, lunch and dinner, as well as drinks. Chill out areas with bean bags will also be available.

##What can't I bring?
Alcohol, cigarettes and vapourisers cannot be consumed in the venue. Objects that will disrupt other teams are also banned, such as speakers for playing loud music.

##Preparing for UHack
During UHack weekend you'll be working on your Business Model Canvas, your app and your pitch video. You must not do any of this work prior to the event.

Leading up to the event it is fine for your team to start thinking about the problems and opportunities you want to address. Do your research into the University Research Themes and spend some time digging around the University website.

It's also a good idea to learn about the Business Model Canvas and do some research into preparing pitch videos. We'll be running some seminars on these leading up to UHack weekend – keep an eye out on the UHack website and the University Event Calendar for details – stay tuned!

Finally, it's a very good idea for your team to familiarise yourself with your development and video editing tools, and with your hosting platform of choice before UHack weekend!

##How do we register our team?
UHack team registrations are handled through Eventbrite. We'll be opening registrations in August, so check back then for details.

<!--
[https://uhack-2016.eventbrite.com.au](https://uhack-2016.eventbrite.com.au)
-->
You will need to create a team on Eventbrite (nominate someone as team captain), then book your tickets. Team sizes are limited to 6 persons. UHack is running simultaneously in Hobart, Burnie and Launceston, so make sure you book the appropriate tickets!

##When will judging occur and prize winners announced?
Entries will be judged by our panel and prizes given at the end of UHack weekend, later in the afternoon of Sunday 15/7. The judges will view every teams' pitch videos and ask the teams questions.

There will also be a People's Choice award made at the end of UHack weekend.