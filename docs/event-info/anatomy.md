# What your team will do during UHack weekend

Your team will need to produce three main deliverables during UHack weekend:

1. A **Business Model Canvas** (BMC) that lays out the elements of how your business idea and app could be brought to reality. We'll provide plenty of BMC worksheets for you to use during the weekend. When you are finished you'll have to upload an image of your canvas somewhere it can be accessed by our judges, (e.g. Google Docs, OneDrive, DropBox), and link to it from your DevPost project page. You will also need to summarise your Canvas in your pitch video.

2. A **Software Application**. This can be anything, e.g. a website, a mobile app, a game. You'll need to host your app somewhere that it can be viewed or downloaded during UHack weekend (and longer if possible), and link to it from your DevPost project page. We suggest you use one of the many free or trial services available from the likes of Google, Azure, OpenShift, Heroku, GitHub, etc. (NB: UHack does not provide any app hosting facilities.) **Tip: your team should familiarise themselves with your hosting platform before the competition weekend.**

3. A **3-Minute Pitch Video**. During your video you'll summarise your Business Model Canvas and demo your app. You'll need to shoot and edit your video using your own equipment – laptop and phone camera quality will be fine. You'll need to upload your video to YouTube (or similar) for our judges to view, and link to it from your DevPost project page.

**Important: your pitch video is the main vehicle used by our judges to score your entry. Due to time constraints there may not be the chance to demonstrate your app directly to the judges, so it is important that your pitch video does a good job of demoing your app as well as summarising your business model. Also, make sure you tie your entry to one or more of the University's Research Themes.**

What you develop is up to you – so long as the problem and opportunity that you identify aligns with the University Research Themes:

* Environment, Resources and Sustainability
* Creativity, Culture and Society
* Better Health
* Marine, Antarctic and Maritime
* Data, Knowledge and Decisions



It's useful to bear in mind that the competition judges will be focused on the tangible outcomes of your project, so making your project page a snazzy and useful resource with information about your project, business model, screenshots, your 3 minute video, and anything else that shows off how *awesome* your project is is REALLY important :)


## Register your project and team

Firstly, get one of your team to sign up and register your team on [DevPost](http://uhack-tasmania-2017.devpost.com/). You **need to have registered** your team and created your project page by midday Saturday (local time), but you're free to continue editing and improving it until the competition closes.

If you experience any issues with registering your team, or have any questions about what is required of you, seek out one of your friendly local UHack organisers and they'll give you a hand.

[How to enter a submission](https://help.devpost.com/customer/en/portal/articles/1389689-enter-a-submission)

## Prepare your video

An important part of your project is the 3 minute pitch video explaining our business model and showing your application in action that you'll make to show off your project to the competition judges.

The preferred method is to use a screencast with a voice-over narration explaining your project, your business model, and what is being show in the video. Remember that the judging panel is viewing the videos in isolation and doesn't necessarily have any context around your project.

You may mix in other elements with the screencast, such as footage demonstrating the issues your projects addresses, interviews, live action material and actors (read: team members and bribeable friends!) you've filmed, et cetera - but be aware that videos that don't primarily focus on showing off the project itself will not be as valued as ones that do.

You are encouraged to include your team name, team members, and to talk about how your project is innovative, has impact and relates to the University's Research Themes.

Check out the hacker toolkit for some assistance and instruction on how to make a compelling video. Remember: Your video should not take more than a few hours out of your weekend if you keep it simple.

### Storyboarding, screencasting, and editing

To help with storyboarding your video grab this huge pack of [free storyboarding illustrations](https://dribbble.com/shots/1083617-430-FREE-storyboard-illustrations).

For screencasting check out software like [ActivePresenter](http://atomisystems.com/activepresenter/free-edition/) that will allow you to record demos of your application.

For mixing clips together the [YouTube Video Editor](https://www.youtube.com/editor) is super user-friendly; though [VLC](http://www.videolan.org/vlmc) or [LWKS](http://www.lwks.com/) may also be handy.


And again, if you are unsure about what you need to do, or just need a bit of help with your video, hunt down one of your local UHack organisers and they'll be happy to help.


## Submit your project

The last tenet of UHack is that you submit all of your source code and assets (data, documents, art assets, et cetera). Typically this will comprise the source code for a web or mobile application, but for other types of works that can be your notes and evidence of your prototypes.

The key point to remember is that your source material needs to demonstrate to the competition judges that the end result was your own team's work done during UHack.

You're free to submit your source materials in any fashion, but typically we find people like to use [GitHub](https://github.com/) or [BitBucket](https://bitbucket.org/). Both of these services are free for open source projects and have user-friendly web and desktop applications to allow even novice users to create, submit, and edit their source material.

GitHub has some useful guides [here](https://guides.github.com/activities/hello-world/).

Bitbucket has some useful guides [here](https://www.atlassian.com/git/tutorials/)

The simplest way to submit the source materials is to put a link to your GitHub repository on your project page in DevPost.