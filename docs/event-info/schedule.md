


Friday 13 July 

|Time|Event|
|---|---|
|5:00pm|Registration Opens|
|6:00pm|Opening Ceremony|
|7:00pm|HACKING BEGINS|
|7:00pm|Dinner|
|11:00pm|Venues close for night|

Saturday 14 July

|Time|Event|
|---|---|
|7:00am|Venues Reopen|
|8:00am|Breakfast|
|10:00am|Morning Tea|
|1:00pm|Lunch|
|3:00pm|Afternoon Tea + Project Updates - each team to give a short update on their project|
|6:00pm|Dinner|
|11:00pm|Venues close for night|

Sunday 15 July

|Time|Event|
|---|---|
|7:00am|Venues Reopen|
|8:00am|Breakfast|
|10:00am|Morning Tea|
|1:00pm|Lunch|
|2:00pm|HACKING STOPS - Projects to be finalised in DevPost|
|2:00pm-4:00pm|Viewing of pitch videos + Judging + Peoples' Choice vote|
|4:30pm|Winners announced|
|5:00pm|Closing Ceremony|
|6:00pm|Goodbye|
