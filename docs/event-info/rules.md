

#UHack Rules

##1. Submitting

1.1 DevPost is the Official UHack competition submission site and allows you to submit all components required for your team’s UHack entry. Note: submission elements and times are system controlled so no extensions are available!

1.2 Register at least one team member in DevPost (you'll need at least one person registered in DevPost to create and submit your project page).

1.3 Teams are required to submit the following as part of their competition entry on DevPost:

1.3.1   A descriptive project page in DevPost, listing your team members, with details about your project.

1.3.2   A Business Model Canvas (BMC) that lays out the elements of how your business idea and app could be brought to reality. We'll provide plenty of BMC worksheets for you to use during the weekend. When you are finished you'll have to upload an image of your canvas somewhere it can be accessed by our judges, (e.g. Google Docs, OneDrive, DropBox), and link to it from your DevPost project page.

1.3.3   A Software Application. This can be anything, e.g. a website, a mobile app, a game. You'll need to host your app somewhere that it can be viewed or download by our judges, and link to it from your DevPost project page. We suggest you use one of the many free or trial services available from the likes of Google, Azure, OpenShift, Heroku, GitHub, etc. (NB: UHack does not provide any app hosting facilities.) Tip: your team should familiarise themselves with your hosting platform before the competition weekend.

1.3.4   A Pitch Video (maximum three minutes) that explains your Business Model Canvas and demos your app. You'll need to shoot and edit your video using your own equipment – laptop and phone camera quality will be fine. You'll also need to upload your video to YouTube (or similar) for our judges to view, and link to it from your DevPost project page. Remember that the judging panel is viewing the videos in isolation and doesn’t necessarily have any context around your project. You may mix in other elements with the screen-cast, such as footage demonstrating the issues your projects addresses, interviews, live action material you’ve filmed, et cetera – but be aware that videos that don’t focus on showing off the project itself will not be as valued as ones that do. Check out the hacker toolkit for some assistance and instruction on how to make a compelling video. Remember: Your video should not take more than a few hours out of your weekend if you keep it simple.

1.3.5   Outcomes from the project itself (any code, graphics, mashups, applications, website URLs, photos of each stage to create your artistic representation etc.). If judges are able to see and play with it that is useful, but this is a minor component of the judging. Teams can put the code/source on GitHub, Bitbucket, Sourceforge or an equivalent repository system and must make the URL available on their project page for verification. For artistic works you may need to create a photo library or share a link to a Googledocs or similar that contains evidence of the stages of your project.

##2. Divisions

* Open Division: is open to student and public teams. Student teams are automatically eligible for Open Division prizes.
* Student Division: open to all-student teams only. Students are eligible for both the open and student divisions, but can only win prizes in one division.

2.1 You must nominate which division you are competing for on your DevPost project page.

2.2 A Peoples’ Choice award will be issued to the highest voted overall project by participants on Sunday afternoon. Only registered team members can vote so make sure all your team members are registered on your project page.

2.3 Each UHack location will have a Local Spirit of UHack prize for the team or individual that displays the greatest spirit of UHack, aka the best “hacker” ethos. This means the team who best helped others, shared, learned or applied their skills creatively or cleverly. Local Spirit prizes are announced on the Sunday night after the competition.

##3. Judging
3.1 All UHack entries will be judged by the UHack Competition Judging Panel against the following criteria:

3.2 The Business Model Canvas (15%)

Are you able to get your business model on one page, that makes it easy to understand: the unmet need, the users / customers, what's unique about your solution, how you will get it your solution to the users, how you expect to cover your costs and beyond, and what's your unfair advantage? Your business model canvass will also show how your solution aligns with one of the University's research themes.

3.3 Your App (50%)

How well executed is your app, game or website, etc.? What is the User Experience like? Have you used any unique technical approaches? Is it accessible? How is your code and solution structured? Hint: If you don't have a working app, do you have detailed concept designs?

3.4 The Pitch Video (15%)

Are you able to communicate your business canvas, demonstrate your app, and engage would-be users in 3 minutes? Can you use the video medium to create excitement and demand for your solution? What evidence can you provide or discuss that adds credibility to your team and your solution? Do you need more resources to take it to the next level – would your video attract crowd funding, the interest of an angel investors or philanthropist – or just get you on the news?

3.5 Innovation (10%)

Do you solve a problem or capture an opportunity in a creative or never-seen-before way? How novel is your idea?

3.6 Potential for Impact (10%)

Would the application and associated plan have real impact (economic, societal, environmental)? Can it change the world, or at least help someone in your community? Does it solve a problem aligned with the University's research themes?

3.7 The UHack competition judges will choose all winners. The judging panel for prizes will consist of a mix of UHack organisers, University representatives and industry sponsors as appropriate for each prize. All submission elements detailed in “Submitting” section of this document must be completed by the required time. No requests for extensions will be considered. Final arbiter is the judging panel whose decision is final. No correspondence will be entered into. This is a competition of skill. Chance plays no part in this competition. Judges are not eligible to compete for prizes.

##4. General

4.1 Each winning team must nominate one person to liaise with and provide their details to UHack organisers following prize announcements to coordinate distribution of prizes after the event and prize money must be evenly split between all team members of winning teams. If all members of your team are under 18 then please nominate a guardian.

4.2 Some local prizes may be handed out on the Sunday afternoon, but most prizes will be announced in the week following UHack.

4.3 To be eligible for prizes at least one member of the team must be an Australian or New Zealand citizen or a current Australian or New Zealand resident (this includes temporary student residents). It’s only fair – it is a University of Tasmania UHack competition after all. At least one team member must be over 18 (or a guardian must be registered as the representative to facilitate prizes).

4.4 Judges expect entries to be primarily developed throughout the weekend of UHack. If submissions are shown to have been worked on before the weekend, the submission will be ineligible for prizes. This does not include reuse or extension of existing software, libraries or data sets. Each team must be registered separately and each team has one entry.

4.5 The maximum team size is 6.

4.6 No judges will be eligible to compete for prizes, and individuals from organisations or companies are also not eligible for prizes sponsored by their organisation. Mentors/speakers are eligible to compete for prizes, but judges reserve the right to disqualify a mentor/speaker if they perceive unfairness.

4.7 Don’t do bad things. Please participate in and engage with the contest in that spirit and in good faith. You must not include submissions that are:

4.7.1   potentially libellous, false, defamatory, privacy invasive or overtly political;

4.7.2   material which is potentially confidential, commercially sensitive, or which would cause personal distress or loss;

4.7.3   any commercial endorsement, promotion of any product, service or publication;

4.7.4   language which is offensive, obscene or otherwise inappropriate; or misleading, deceptive, violate a third party’s rights or are otherwise contrary to law.

4.8  We reserve the right to reject submissions that do not comply with the letter and spirit of these rules.

4.9 You agree to only include code, data, or other materials in a submission for the UHack contest that you have the right to use and release consistent with these Contest Rules.

4.10    The University of Tasmania does not claim any ownership of the Intellectual Property teams submit during UHack, nor can we provide any protection of IP or sensitive material. You own what you produce at UHack. We highly recommend you discuss the ownership of IP and division of prizes with your team members before the Hackathon begins.

4.11    Entrants consent to the University of Tasmania using their name, likeness, image and/or voice in any media for an unlimited period of time, without remuneration, for any publicity and marketing purposes.

4.12    Entrants grant permission to the University of Tasmania to use and reproduce any video or images produced during UHack for an unlimited period of time, without remuneration, for any publicity and marketing purposes.

4.13    Submissions and comments will be posted live, but occasionally they may not make it through our anti-trolling and anti-spamming filters and may need to be moderated manually. We reserve the right to remove or not post any submission that reasonably appears to breach any of these rules.

4.14    The UHack team nor the University of Tasmania makes no representations or warranties of any kind, expressed or implied, including warranties of accuracy, in regard to any submissions or links published on the UHack website.

##5. Personal Information, Photographs and videos

5.1 The University of Tasmania (“the University”) may take pictures of you or record you while you are at the UHack event. The purpose for which the University may use your picture or record you is for marketing purposes, and the images or videos may be displayed on the University’s website, the UHack website, or on the University’s social media pages, including Facebook, Twitter and Instagram.  The University will not disclose your pictures of video recordings for any other purpose other than what has been described in this clause. You consent to the University to use your picture, video recording or image for the purposes outlined in this clause. You may contact the University on 03 6226 1818 to discuss how your personal information (such as your picture or video recordings) are being used, disclosed or stored. You have the right to request information that the University holds under the Right to Information Act 2009 (Tas). For further information regarding how the University collects, holds, uses and discloses personal information, please refer to the [University’s Privacy Policy](http://www.utas.edu.au/privacy).

5.2 Personal Information

5.2.1 For further information on how your Personal Information collected during UHack is being used, disclosed and stored – or to update your Personal Information contact service.desk@utas.edu.au or phone 03 6226 1818.

5.2.2 You have the right to request information we hold under the provisions of the Right to Information Act 2009.

5.2.3 This information is collected for assessing, allocating and administering judging and prizes for the UHack competition and promotion of that competition.

5.2.4 The intended recipients of this information are the UHack coordinators, judging panel, University of Tasmania.

5.2.5 If this information is not provided you may not be eligible to participate in UHack.

##6. UHack Code of Conduct

6.1 UHack should be an awesome experience for everyone. Be nice, play fair, or go home.

6.2 By participating in UHack, as an observer or a competitor, you agree to the following:

6.2.1   I will treat others with respect

6.2.2   I will not abuse, stalk, harass or threaten others. I will not make offensive comments related to gender, sexual orientation, disability, physical appearance, body size, race, or religion. I will not disrupt other people or the event and I will not make inappropriate physical contact or pay unwelcome sexual attention to other participants.

6.2.3   I will keep it G-rated and be mindful of language

6.2.4   I will not swear or make sexist, racist, or other exclusionary jokes, which may be offensive to those around me. I will behave in a way and only submit competition material that is suitable for anyone to view, including young children.

6.2.5   I will respect the venue and equipment

6.2.6   I will keep the venue clean and tidy and use the rubbish and recycling bins as appropriate. I will let the organisers know if there are any issues.

6.2.7   I shall follow the competition rules

6.2.8   I will only use authorised materials that I have the right to use and release and will not submit any projects which are potentially libellous, false, defamatory or overtly political or contains material which is potentially confidential, commercially sensitive, or which would cause personal distress or loss. I will check the website and/or have a chat with one of the organisers if at any stage I am unclear on the competition rules.

6.2.9   I shall look after others and myself

6.2.10  I will remember that we are all here to create, not party. If I am under 18, I will ensure my guardian is with me whenever I am at the venue, and if I am the guardian of a participant under 18 I will ensure I keep an eye on them. If I am feeling uncomfortable, am being harassed, notice that someone else is being harassed, or have any other concerns, I will immediately contact a UHack organiser.

6.2.11  And remember that the UHack crew are in charge and have put a lot of effort in to organising a great event.

6.2.12  I will not do anything to ruin it or engage in any behaviour that violates this code of conduct. I understand that the UHack organisers may take any action they deem appropriate, including warnings or expulsion from the UHack event. I understand that if I am removed from the event due to inappropriate behaviour, I will no longer be eligible to compete for UHack prizes.

