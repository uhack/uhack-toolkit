# What your team will do during UHack weekend

Your team will need to produce three main deliverables during UHack weekend:

1. A Business Model Canvas (BMC) that lays out the elements of how your business idea and app could be brought to reality. We'll provide plenty of BMC worksheets for you to use during the weekend. When you are finished you'll have to upload an image of your canvas somewhere it can be accessed by our judges, (e.g. Google Docs, OneDrive, DropBox), and link to it from your DevPost project page.

2. A Software Application. This can be anything, e.g. a website, a mobile app, a game. **You'll need to host your app somewhere that it can be viewed or download during UHack weekend (and longer if possible), and link to it from your DevPost project page.** We suggest you use one of the many free or trial services available from the likes of Google, Azure, OpenShift, Heroku, GitHub, etc. (NB: UHack does not provide any app hosting facilities.) **Tip: your team should familiarise themselves with your development tools and hosting platform before the competition weekend.**

3. A 3-Minute Pitch Video. During your video you'll summarise your Business Model Canvas and demo your app. You'll need to shoot and edit your video using your own equipment – laptop and phone camera quality will be fine. You'll need to upload your video to YouTube (or similar) for our judges to view, and link to it from your DevPost project page.

**Important: your pitch video is the main vehicle used by our judges to score your entry. Due to time constraints there may not be the chance to demonstrate your app directly to the judges, so it is important that your pitch video does a good job of demoing your app as well as summarising your business model. Also, make sure you tie your entry to one or more of the University's Research Themes.**

What you develop is up to you – so long as the problem and opportunity that you identify aligns with the [University Research Themes](http://www.utas.edu.au/research/our-research-themes):

* Environment, Resources and Sustainability
* Creativity, Culture and Society
* Better Health
* Marine, Antarctic and Maritime
* Data, Knowledge and Decisions

Make sure that you tie your project to one or more of these research themes - do your research on the UTAS website, etc. This is an important element in judging.

It's useful to bear in mind that the competition judges will be focused on the tangible outcomes of your project, so making your project page a snazzy and useful resource with information about your project, business model, screenshots, your 3 minute video, how it releates to the research themes, and anything else that shows off how *awesome* your project is is REALLY important :)

## Register your project and team on DevPost and create a project page

Firstly, get at least one of your team to sign up and register your team on [DevPost](http://uhack-tasmania-2018.devpost.com/). You will need to create a project page in DevPost - this is where you'll describe your project, and link to your project outputs. You **need to have registered** your team and created your project page by midday Saturday, but you're free to continue editing and improving it until the competition closes.

If you experience any issues with registering your team, or have any questions about what is required of you, seek out one of your friendly local UHack organisers and they'll give you a hand.

[How to submit in DevPost](https://help.devpost.com/customer/en/portal/articles/1389689-enter-a-submission)
<iframe width="100%" height="360" src="https://www.youtube.com/embed/vCa7QFFthfU?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe>

## Create your Software Application

A key element of you project is of course your software application that you'll create over the weekend. As pointed out above, you'll need to link to your application from your DevPost project page. You'll need to ensure that your app is accessible by the judging panel during the week following UHack weekend, for them to download or view via the public Internet. If you can't do this, make sure you include plenty of screenshots of your app on your DevPost project page. Include information that describes or explains you app on your project page.

## Create your Business Model Canvas and Value Proposition Canvas

Document your business model for your project using the supplied Business Model Canvas and Value Proposition Canvas worksheets. You can find more information about BMC on the [BMC Resource Page](/resources/bmc.md). When you're finished, take a photo of your worksheets and upload the images to a free file storage service, e.g. Google Docs, OneDrive, Dropbox, making sure the images are accessible to anyone over the Internet. Link to the images from your project page. Make sure you also describe your business model on your project page too.

## Prepare your video

An important part of your project also is the 3 minute pitch video explaining your business model and showing your application in action that you'll make to show off your project to the competition judges.

The preferred method is to use a screencast with a voice-over narration explaining your project, your business model, and what is being shown in the video. Remember that the judging panel is viewing the videos in isolation and doesn't necessarily have any context around your project.

You may mix in other elements with the screencast, such as footage demonstrating the issues your projects addresses, interviews, live action material and actors (read: team members and bribeable friends!) you've filmed, et cetera - but be aware that videos that don't primarily focus on showing off the project itself will not be as valued as ones that do.

You are encouraged to include your team name, team members, and to talk about how your project is innovative, has impact and relates to the University's Research Themes.

Check out the [pitch](/resources/pitch.md) section for some points on how to make a compelling video. Remember: Your video should not take more than a few hours out of your weekend if you keep it simple.


## Submit your source material

The last tenet of UHack is that you submit all of your source code and assets (data, documents, art assets, et cetera). Typically this will comprise the source code for a web or mobile application, but for other types of works that can be your notes and evidence of your prototypes.

The key point to remember is that your source material needs to demonstrate to the competition judges that the end result was your own team's work done during UHack.

You're free to submit your source materials in any fashion, but typically we find people like to use [GitHub](https://github.com/) or [BitBucket](https://bitbucket.org/). Both of these services are free for open source projects and have user-friendly web and desktop applications to allow even novice users to create, submit, and edit their source material.

GitHub has some useful guides [here](https://guides.github.com/activities/hello-world/).

Bitbucket has some useful guides [here](https://www.atlassian.com/git/tutorials/)

The simplest way to submit the source materials is to put a link to your GitHub repository on your project page in DevPost.