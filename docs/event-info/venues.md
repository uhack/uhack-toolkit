UHack 2018 is being held simultaneously in Hobart, Burnie and Launceston.

Hobart:

University of Tasmania,
Centenary Building,
Harvard Room LT2 (Ground Floor),
Sandy Bay Campus, Churchill Avenue,
Sandy Bay 7005

Launceston:

University of Tasmania,
Architecture Tutorial Room 1-2, 
Inveresk Campus,
Launceston 7250

Burnie:

University of Tasmania, 
Domestic Arts Building, 
West Park Precinct, 
2 – 4 Bass Highway,
Burnie, 7320

<!--

#Hobart

The Old Mercury Building  
91-93 Macquarie Street  
Hobart TAS 7000  

[Open in Google Maps](https://goo.gl/maps/VKxYpqxTo9z)

##How do I get there?

The Old Mercury Building is around the corner from the Hobart Bus mall. Catching any bus that goes to Hobart will get you there!

##Parking
The nearest parking stations are Argyle Street Carpark (closes at 10:00PM) and Market Place (24HR).

#Launceston

University of Tasmania   
School of Architecture & Design  
8 Invermay Road  
Inveresk Campus, Launceston TAS 7250  

[Open in Google Maps](https://goo.gl/maps/7sLrbEPY4Gw)

##How do I get there?

Take any bus that heads to Invermay, Mowbray or Inveresk Campus (routes 6, 7, 8 or 10). Get off at first stop over the Tamar Street bridge. You can even walk there from town, as it is a short walk (use the Google Map linked).

-->