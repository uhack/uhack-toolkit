#DevPost
UHack utilises DevPost to manage project submissions for judging. DevPost is a free service for managing hackathons and project pages.

The UHack 2017 DevPost page is here: [uhack-tasmania-2018.devpost.com/](http://uhack-tasmania-2018.devpost.com/).

##You will need to do the following:

* Register at least one person for the UHack hackathon in DevPost. To do this click the [Register for this hackathon] button on the UHack page, and sign up. Once you have created a DevPost account you will be able to create project pages.
* Create a DevPost project page for your UHack project - you can do this via the [Enter a submission] button on the UHack page, or via your Portfolio in DevPost.
* Submit your DevPost project to the UHack 2016 hackathon

##When creating and editing your DevPost project make sure you:

* Invite your team members onto the project via email.
* Describe your project thoroughly in the "Here's the whole story" section. You use Markdown for this - check out this [Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) for a quick introduction to Markdown formatting.
* Put the links to your app and other resources (such as your BMC, screengrabs, etc.) in the "Try it out" field. You can add multiple links.
* Upload images, such as screenshots, in the "Image Gallery"
* Put the URL of your pitch video in the "Video demo"

You can also upload a zip file when you submit your project.

##Submitting your project 
[How to submit in DevPost](https://help.devpost.com/customer/en/portal/articles/1389689-enter-a-submission)
<iframe width="100%" height="360" src="https://www.youtube.com/embed/vCa7QFFthfU?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe>





