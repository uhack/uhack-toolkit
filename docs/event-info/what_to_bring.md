##What should I bring?
You'll need to bring all the equipment you need to do your work, including laptops, mobile phone, chargers, etc. Make sure you bring warm clothes too.<!--and a change of clothes.-->

Make sure you also bring the cables that connect your phones to your computers - you'll need these to transfer videos and pictures. 

<!--We suggest bringing a sleeping bag and toiletries if you want to stay the night.--> 

##What is provided?
We'll provide WiFi, breakfast, snacks, lunch and dinner, as well as drinks. Chill out areas with bean bags will also be available.

##What can't I bring?
Alcohol, cigarettes and vapourisers cannot be consumed in the venue. Objects that will disrupt other teams are also banned, such as speakers for playing loud music.

##Can I leave equipment overnight?
We discourage this, however recognise some people like to have their monitor at the events. Please be aware that you do this at your own risk and UHack accepts no responsibility for any possessions left unattended at the venue.

