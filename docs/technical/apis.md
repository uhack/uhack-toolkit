## On the importance of APIs

There's a good chance that your project is going to need some sort of API to connect up your backend layer or data store to your frontend interface (be it web-based or otherwise). But APIs are hard, right? Not any more! Where once we used to have to roll our own APIs and handle the lowest level operations ourselves now there are a bevvy of tools and resources to help you make an awesome API quickly.

### APIs as a Service

Services like [Mashery](http://www.mashery.com/), [Apigee](https://apigee.com/about/), [ApiAxle](http://apiaxle.com/), and [3scale](http://www.3scale.net/) provide a quick and easy means to bootstrap your API and provide a range of neat features on the side like analytics and easy inspection and diagnosis of API errors.

### Building and designing your own API

If you need to dig a little further down and actually build your own API it's worth taking a bit of time to think about the design of your API. There are some great resources are such as the API Evangelist's guide to [Providing and Consuming APIs](http://apievangelist.com/) and Atlassian's [REST API Design Guidelines](https://developer.atlassian.com/docs/atlassian-platform-common-components/rest-api-development/atlassian-rest-api-design-guidelines-version-1).

[apiblueprint](http://apiblueprint.org/) is well worth a look as a tool that provides both a means of quickly designing an API, as well as automatically generating nice looking documentation.

### Documenting your API

Documenting your API well is crucial if you expect anyone else to use it (including other developers on your team) and the tools available are myriad.

For REST APIs in general check out [Swagger](http://swagger.io/) or [iodocs](https://github.com/mashery/iodocs). In PHP-land [Symfony](http://symfony.com/) (see also - [REST APIs with Symfony2: The Right Way](http://williamdurand.fr/2012/08/02/rest-apis-with-symfony2-the-right-way/)) and [Slim](http://www.slimframework.com/) are worth a look

In Rails-land take a look at [Apipie-rails](https://github.com/Apipie/apipie-rails).