# Business Model Canvas

The Business Model Canvas (BMC), is a strategic management and entrepreneurial tool. It allows you to describe, design, challenge, invent, and pivot your business model.

There are many resources on the web about the Business Model Canvas, here's a [Google search](https://www.google.com.au/webhp?q=business+model+canvas) to start you off.

[YouTube](https://www.youtube.com/results?search_query=business+model+canvas) has many very good video resources too.

A good starting place is on the Strategyzer site: [https://strategyzer.com/canvas](https://strategyzer.com/canvas). Strategyzer have a lot of free resources on their site, but you'll need to register to access them.

Here's a short introduction to the BMC:

<iframe width="425" height="254" src="http://www.youtube.com/embed/QoAOzMTLP5s" frameborder="0" allowfullscreen="true"></iframe>

<br/>

Here's a good introduction by Simon Boot:

<iframe width="425" height="254" src="https://www.youtube.com/embed/7mEISPNVv3I" frameborder="0" allowfullscreen="true"></iframe>

<br/>
And here's a full introduction by the author of the BMC, Alex Osterwalder:

<iframe width="425" height="254" src="https://www.youtube.com/embed/RzkdJiax6Tw" frameborder="0" allowfullscreen="true"></iframe>

<br/>
A good starting point with BMC is to start with your value proposition and product-market fit:

<iframe width="425" height="254" src="https://www.youtube.com/embed/aN36EcTE54Q" frameborder="0" allowfullscreen="true"></iframe>

<br/>
[Fourteen Essential Links for working with the Business Model Canvas](http://blog.strategyzer.com/posts/2016/8/8/14-essential-links-for-working-with-the-business-model-canvas)


[Business Model TV](https://www.youtube.com/user/businessmodeltv/videos) - Lots of good videos on BMC.

Finally, there’s also a free course by Udacity on [how to build a startup using BMC](
https://www.udacity.com/course/how-to-build-a-startup--ep245).

[Business Model Canvas - A3 PDF](/resources/Business_Model_Canvas_A3.pdf)

[Value Proposition Canvas PDF](/resources/Value_Proposition_Canvas.pdf)
