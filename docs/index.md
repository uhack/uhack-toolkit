
![UHack](/imgs/uhack-2018-large.jpg "UHack")


#The UHack Guide

This site provides much of the information you'll need to prepare your [UHack](http://www.utas.edu.au/uhack) competition entries, and links to resources to help you create an awesome project. 

Read the [Your UHack Project](/event-info/project/) section to find out all about the process of registering, preparing, and submitting your project, and the [Rules](/event-info/rules) section.

Look through the resources section for tips on how to build any sort of project - mobile apps, web apps, APIs, data visualisations/infographics, and more. 

You'll find a range of useful resources from [how best to put your entry online](/resources/project-hosting/), to [suggestions of tools to use to maximise your hacking productivity](/resources/productive-hacking/), and resources for working with data.

There are also links to resources to help you build your [Business Model Canvas](/resources/bmc), and tips on preparing your [Pitch Video](/resources/pitch).

Remember to check the main [website](http://www.utas.edu.au/uhack) for updates.

#DevPost

UHack utilises DevPost to manage project submissions for judging. DevPost is a free service for managing hackathons and project pages.

The UHack 2018 DevPost page is here: [uhack-tasmania-2018.devpost.com/](http://uhack-tasmania-2018.devpost.com/).

Make sure you familiarise yourself with the DevPost submission process - you can find more details in the [DevPost section](/event-info/devpost/) of this guide.

# UHack Schedule

UHack runs from the evening of Friday 13 July through to early evening Sunday 15 July. Check out the [Schedule](/event-info/schedule) section for more details.

#People's Choice Award
Vote [Here](https://goo.gl/forms/iNG3C2UUkv5CmAPc2)


<!--
Saturday August 27

|Time|Event|
|---|---|
|8:30am|Doors open for registration|
|10:00am|Opening Ceremony + Morning Tea|
|10:30am|HACKING BEGINS|
|1:00pm|Lunch|
|3:00pm|Afternoon Tea + Project Updates - each team to give a short update on their project|
|6:00pm|Dinner|
|10:00pm|Doors locked for participants|

Sunday August 28

|Time|Event|
|---|---|
|12:00am|Midnight Snack|
|7:00am|Doors reopen for participants|
|8:00am|Breakfast|
|10:00am|Morning Tea + Project Updates - each team to give a short update on their project|
|1:00pm|Lunch|
|4:00pm|HACKING STOPS - Projects to be finalised in DevPost|
|4:00-5:00pm|People's Choice - viewing of pitch videos + vote|
|5:00pm|Closing Ceremony|
|6:00pm|Goodbye|
-->

