
##Personal Information

For information regarding how the University collects, holds, uses and discloses personal information, please refer to the [University’s Privacy Policy](http://www.utas.edu.au/privacy).

(a) For further information on how your Personal Information collected during UHack is being used, disclosed and stored – or to update your Personal Information contact service.desk@utas.edu.au or phone 03 6226 1818

(b) You have the right to request information we hold under the provisions of the Right to Information Act 2009

(c) This information is collected for assessing, allocating and administering judging and prizes for the UHack competition and promotion of that competition

(d) This intended receipents of this information are the UHack coordinators, judging panel, University of Tasmania 

(e) If this information is not provided you may not be eligible to participate in UHack


The text of this guide is open for reuse under a [Creative Commons Attribution](https://creativecommons.org/licenses/by/3.0/au/deed.en) license.

This guide is based on and adapted from the [GovHack Hacker Toolkit](http://govhack-toolkit.readthedocs.io/). We register our thanks to the [GovHack](http://www.govhack.org/) Team!